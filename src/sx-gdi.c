/* -*- Mode: C; indent-tabs-mode:nil; c-basic-offset: 8-*- */

/*
 * This file is part of The SEWFOX project
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2.1 of the 
 * GNU Lesser General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the 
 * GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * Author: Dodji Seketeli
 * See COPYRIGHTS file for copyright information.
 */

#include <string.h>
#include "sx-gdi.h"

#define PRIVATE(obj) (obj->priv)

/**
 *Private attributes of the SXGdi Interface;
 */
struct _SXGdiPrivate {
        /**
         *User data used by the eventual Graphical Device Interface 
         *implementation.
         *This must be get/set using sx_gdi_get_private_data() and
         *sx_gdi_set_private_data().
         */
	gpointer private_data ;

	/********************************************************
	 *Graphical Device Interface function pointers (or handlers)
         *These must be set to their actual implementation
         *by the implementation of the Graphical Device Interface.
         *Setting these handlers must be done by the matching
         *sx_gdi_set_handler_*() method.
	 ********************************************************/

	SXGdiDestroyHandler destroy_handler;
	SXGdiGetXDpi get_xdpi ;
	SXGdiGetYDpi get_ydpi ;
	SXGdiLayoutTextInBox layout_text_in_box ;
	SXGdiGetTextLayoutPixelExtents get_text_layout_pixel_extents ;
        SXNbEmToTextFontSize nb_em_to_text_font_size ;
        SXPredefinedAbsoluteFontSizeToPointSize predefined_absolute_font_size_to_point_size ;
} ;

/*********************
 *Gdi public apis
 *********************/

SXGdi *
sx_gdi_new (gpointer a_private_data)
{
	SXGdi *result = NULL ;

	result = g_try_malloc (sizeof (SXGdi)) ;
	if (!result) {
		cr_utils_trace_info ("Out of memory") ;
		return NULL ;
	}
	memset (result, 0, sizeof (SXGdi)) ;
	PRIVATE (result) = g_try_malloc (sizeof (SXGdiPrivate)) ;
	if (!PRIVATE (result)) {
		cr_utils_trace_info ("Out of memory") ;
		g_free (result) ;
		return NULL ;
	}
	memset (PRIVATE (result), 0, sizeof (SXGdiPrivate)) ;
	if (a_private_data) {
		sx_gdi_set_private_data 
			(result, a_private_data) ;
	}
	return result ;
}

enum SXGdiStatus
sx_gdi_set_private_data (SXGdi *a_this,
			 gpointer a_priv_data)
{
	g_return_val_if_fail (a_this && PRIVATE (a_this),
			      GDI_BAD_PARAM_ERROR) ;

	PRIVATE (a_this)->private_data = a_priv_data ;
	return GDI_OK ;
}

enum SXGdiStatus 
sx_gdi_get_private_data (SXGdi *a_this,
			 gpointer *a_priv_data)
{
	g_return_val_if_fail (a_this && PRIVATE (a_this),
			      GDI_BAD_PARAM_ERROR) ;
	*a_priv_data = PRIVATE (a_this)->private_data ;
	return GDI_OK ;
}

enum SXGdiStatus
sx_gdi_get_x_dpi (SXGdi *a_this,
		  gint *a_xdpi)
{
	g_return_val_if_fail (a_this && PRIVATE (a_this),
			      GDI_BAD_PARAM_ERROR) ;

	if (PRIVATE (a_this)->get_xdpi) {
		return PRIVATE (a_this)->get_xdpi (a_this, a_xdpi) ;
	} else {
		return GDI_UNDEF_INTERFACE_ERROR ;
	}
}

enum SXGdiStatus
sx_gdi_get_y_dpi (SXGdi *a_this,
		  gint *a_ydpi)
{
	g_return_val_if_fail (a_this && PRIVATE (a_this),
			      GDI_BAD_PARAM_ERROR) ;

	if (PRIVATE (a_this)->get_ydpi) {
		return PRIVATE (a_this)->get_ydpi (a_this, a_ydpi) ;
	} else {
		return GDI_UNDEF_INTERFACE_ERROR ;
	}
}

enum SXGdiStatus
sx_gdi_layout_text_in_box (SXGdi *a_this,
			   SXBox *a_text_box)
{
	g_return_val_if_fail (a_this && PRIVATE (a_this),
			      GDI_BAD_PARAM_ERROR) ;
	if (PRIVATE (a_this)->layout_text_in_box)
		return PRIVATE (a_this)->layout_text_in_box (a_this,
							     a_text_box) ;
	else
		return GDI_UNDEF_INTERFACE_ERROR ;
}

enum SXGdiStatus
sx_gdi_get_text_layout_pixel_extents (SXGdi *a_this,
				      SXBox *a_box,
				      struct SXGdiRectangle *a_rect)
{
	g_return_val_if_fail (a_this && PRIVATE (a_this)
			      && a_rect,
			      GDI_BAD_PARAM_ERROR) ;

	if (PRIVATE (a_this)->get_text_layout_pixel_extents)
		return PRIVATE (a_this)->get_text_layout_pixel_extents 
			(a_this, a_box, a_rect) ;
	else
		return GDI_UNDEF_INTERFACE_ERROR ;
}

enum SXGdiStatus 
sx_gdi_nb_em_to_text_font_size (SXGdi *a_this,
                                CRStyle *a_style,
                                SXBox *a_box,
                                gdouble a_nb_em,
                                glong *a_font_size)
{
        g_return_val_if_fail (a_this
                              && PRIVATE (a_this)
                              && a_box
                              && a_font_size,
			      GDI_BAD_PARAM_ERROR) ;

	if (PRIVATE (a_this)->nb_em_to_text_font_size)
		return PRIVATE (a_this)->nb_em_to_text_font_size
			(a_this, a_style, a_box, 
                         a_nb_em, a_font_size) ;
	else
		return GDI_UNDEF_INTERFACE_ERROR ;
}


enum SXGdiStatus 
sx_gdi_predefined_absolute_font_size_to_point_size (SXGdi *a_this,
                                                    enum CRPredefinedAbsoluteFontSize a_abs_size,
                                                    gdouble *a_point_size)
{
        g_return_val_if_fail (a_this 
                              && PRIVATE (a_this),
                              GDI_BAD_PARAM_ERROR) ;
        
        if (PRIVATE (a_this)->predefined_absolute_font_size_to_point_size) {
                return PRIVATE (a_this)->predefined_absolute_font_size_to_point_size 
                        (a_this, a_abs_size, a_point_size) ;
        } else {
                return GDI_UNDEF_INTERFACE_ERROR ;
        }
}

void
sx_gdi_destroy (SXGdi *a_this)
{
	g_return_if_fail (a_this && PRIVATE (a_this)) ;

	if (PRIVATE (a_this)->destroy_handler)
		PRIVATE (a_this)->destroy_handler (a_this) ;
	g_free (PRIVATE (a_this)) ;
	PRIVATE (a_this) = NULL ;
	g_free (a_this) ;		
}

/****************************************
 *The SXGdi api handler setters/getters
 ****************************************/

void 
sx_gdi_set_handler_destroy_handler (SXGdi *a_this,
                                    SXGdiDestroyHandler a_handler)
{
        g_return_if_fail (a_this && PRIVATE (a_this)) ;

        PRIVATE (a_this)->destroy_handler = a_handler ;
}

void sx_gdi_set_handler_get_xdpi (SXGdi *a_this,
				  SXGdiGetXDpi a_handler)
{
        g_return_if_fail (a_this && PRIVATE (a_this)) ;
        
        PRIVATE (a_this)->get_xdpi = a_handler ;
}

void sx_gdi_set_handler_get_ydpi (SXGdi *a_this,
				  SXGdiGetYDpi a_handler)
{
        g_return_if_fail (a_this && PRIVATE (a_this)) ;

        PRIVATE (a_this)->get_ydpi = a_handler ;
}

void 
sx_gdi_set_handler_layout_text_in_box (SXGdi *a_this,
                                       SXGdiLayoutTextInBox a_handler)
{
        g_return_if_fail (a_this && PRIVATE (a_this)) ;

        PRIVATE (a_this)->layout_text_in_box = a_handler;
}

void sx_gdi_set_handler_get_text_layout_pixel_extents (SXGdi *a_this,
						       SXGdiGetTextLayoutPixelExtents a_handler)
{
        g_return_if_fail (a_this && PRIVATE (a_this)) ;

        PRIVATE (a_this)->get_text_layout_pixel_extents = a_handler ;
}

void 
sx_gdi_set_handler_nb_em_to_text_font_size (SXGdi *a_this,
                                            SXNbEmToTextFontSize a_handler)
{
        g_return_if_fail (a_this && PRIVATE (a_this)) ;

        PRIVATE (a_this)->nb_em_to_text_font_size = a_handler ;
}

void 
sx_gdi_set_handler_predefined_absolute_font_size_to_point_size (SXGdi *a_this,
                                                                SXPredefinedAbsoluteFontSizeToPointSize a_handler)
{
        g_return_if_fail (a_this && PRIVATE (a_this)) ;

        PRIVATE (a_this)->predefined_absolute_font_size_to_point_size = a_handler ;
}
