/* -*- Mode: C; indent-tabs-mode:nil; c-basic-offset: 8-*- */

/*
 * This file is part of The SEWFOX project
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2.1 of the 
 * GNU Lesser General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the 
 * GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * Author: Dodji Seketeli
 * See COPYRIGHTS file for copyright information.
 */

#ifndef __SXGDI_H__
#define __SXGDI_H__

#include "sx-box.h"

typedef struct _SXGdiPrivate SXGdiPrivate ;
typedef struct _SXGdi SXGdi ;

enum SXGdiStatus {
	GDI_OK,
	GDI_BAD_PARAM_ERROR,
	GDI_UNDEF_INTERFACE_ERROR,
        GDI_ERROR
} ;
struct SXGdiRectangle {
	gint x, y, width, height ;
} ;

struct _SXGdi {
	SXGdiPrivate * priv;
} ;

typedef enum SXGdiStatus (*SXGdiDestroyHandler) (SXGdi *a_this) ;

typedef enum SXGdiStatus (*SXGdiGetXDpi) (SXGdi *a_this,
					  gint *a_xdip) ;

typedef enum SXGdiStatus (*SXGdiGetYDpi) (SXGdi *a_this,
					  gint *a_ydpi) ;

typedef enum SXGdiStatus (*SXGdiLayoutTextInBox) (SXGdi *a_this,
						  SXBox *a_text_box) ;

typedef enum SXGdiStatus (*SXGdiGetTextLayoutPixelExtents) (SXGdi *a_this,
							    SXBox *a_box,
							    struct SXGdiRectangle *a_rect) ;

typedef enum SXGdiStatus (*SXNbEmToTextFontSize) (SXGdi *a_this,
                                                  CRStyle *a_style,
                                                  SXBox *a_box,
                                                  gdouble a_nb_em,
                                                  glong *a_font_size);

typedef enum SXGdiStatus (*SXPredefinedAbsoluteFontSizeToPointSize) (SXGdi *a_this,
                                                                      enum CRPredefinedAbsoluteFontSize a_abs_size,
                                                                      gdouble *a_point_size) ;

/******************
 *The SXGdi api
 ******************/

SXGdi *  sx_gdi_new (gpointer a_private_data) ;

enum SXGdiStatus sx_gdi_get_x_dpi (SXGdi *a_this,
				   gint *a_xdpi) ;

enum SXGdiStatus sx_gdi_get_y_dpi (SXGdi *a_this,
				   gint *a_ydpi) ;

enum SXGdiStatus sx_gdi_layout_text_in_box (SXGdi *a_this,
					    SXBox *a_text_box) ;

enum SXGdiStatus sx_gdi_get_text_layout_pixel_extents (SXGdi *a_this,
						       SXBox *a_box,
						       struct SXGdiRectangle *a_rect) ;

enum SXGdiStatus sx_gdi_nb_em_to_text_font_size (SXGdi *a_this,
                                                 CRStyle *a_style,
                                                 SXBox *a_box,
                                                 gdouble a_nb_em,
                                                 glong *a_font_size) ;

enum SXGdiStatus sx_gdi_predefined_absolute_font_size_to_point_size (SXGdi *a_this,
                                                                     enum CRPredefinedAbsoluteFontSize a_abs_size,
                                                                     gdouble *a_point_size) ;

void sx_gdi_destroy (SXGdi *a_this) ;

/******************************************
 *The SXGdi api handlers setters/getters
 ******************************************/

enum SXGdiStatus sx_gdi_set_private_data (SXGdi *a_this,
					  gpointer a_priv_data) ;

enum SXGdiStatus sx_gdi_get_private_data (SXGdi *a_this,
					  gpointer *a_priv_data) ;

void sx_gdi_set_handler_destroy_handler (SXGdi *a_this,
					 SXGdiDestroyHandler a_handler) ;

void sx_gdi_set_handler_get_xdpi (SXGdi *a_this,
				  SXGdiGetXDpi a_handler) ;

void sx_gdi_set_handler_get_ydpi (SXGdi *a_this,
				  SXGdiGetYDpi a_handler) ;

void sx_gdi_set_handler_layout_text_in_box (SXGdi *a_this,
					    SXGdiLayoutTextInBox a_handler) ;

void sx_gdi_set_handler_get_text_layout_pixel_extents (SXGdi *a_this,
						       SXGdiGetTextLayoutPixelExtents a_handler) ;

void sx_gdi_set_handler_nb_em_to_text_font_size (SXGdi *a_this,
                                                 SXNbEmToTextFontSize a_handler) ;

void sx_gdi_set_handler_predefined_absolute_font_size_to_point_size (SXGdi *a_this,
                                                                     SXPredefinedAbsoluteFontSizeToPointSize a_handler) ;

#endif
