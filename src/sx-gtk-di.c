/* -*- Mode: C; indent-tabs-mode:nil; c-basic-offset: 8-*- */

/*
 * This file is part of The SEWFOX project
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2.1 of the 
 * GNU Lesser General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the 
 * GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * Author: Dodji Seketeli
 * See COPYRIGHTS file for copyright information.
 */

#include <string.h>
#include <gtk/gtk.h>
#include "sx-gtk-di.h"

/**
 *@file
 *The definition of the #SXGtkDi class.
 */


struct SXGtkDiPrivate {
	gulong magic ;
	GtkLayout *layout ;
	gulong xdpi ;
	gulong ydpi ;
} ;

const gulong
gv_predefined_abs_font_size_tab[NB_PREDEFINED_ABSOLUTE_FONT_SIZES]
= {
        7,                      /*FONT_SIZE_XX_SMALL */
        9,                      /*FONT_SIZE_X_SMALL */
        11,                     /*FONT_SIZE_SMALL */
        14,                     /*FONT_MEDIUM */
        17,                     /*FONT_LARGE */
        20,                     /*FONT_X_LARGE */
        24                      /*FONT_XX_LARGE */
};

static PangoWeight cr_font_weight_to_pango_font_weight (enum CRFontWeight a_weight) ;

/**************************************************
 *Private methods of the SXGtkDi class
 **************************************************/


/*********************************
 *Below are the GTK implementations of the
 *Graphical Device Interfaces. (SXGdi)
 ***********************************/

static enum SXGdiStatus
destroy_handler (SXGdi *a_this)
{
	enum SXGdiStatus result = GDI_OK ;
	struct SXGtkDiPrivate *thiz = NULL ;

	g_return_val_if_fail (a_this 
			      && sx_gtk_di_is_a (a_this),
			      GDI_BAD_PARAM_ERROR) ;

	result = sx_gdi_get_private_data (a_this, (gpointer)&thiz) ;
	g_return_val_if_fail (result == GDI_OK, GDI_BAD_PARAM_ERROR) ;

	if (thiz) {
		g_free (thiz) ;
		thiz = NULL ;
	}
	return GDI_OK ;
}

static enum SXGdiStatus
get_xdpi (SXGdi *a_this,
	 gint *a_xdpi)
{
	enum SXGdiStatus result = GDI_OK ;
	struct SXGtkDiPrivate *thiz = NULL ;

	g_return_val_if_fail (a_this && sx_gtk_di_is_a (a_this),
			      GDI_BAD_PARAM_ERROR) ;

	result = sx_gdi_get_private_data (a_this, (gpointer)&thiz) ;
	g_return_val_if_fail (result == GDI_OK, GDI_BAD_PARAM_ERROR) ;

	if (!thiz->xdpi) {
		thiz->xdpi = gdk_screen_width () / 
			gdk_screen_width_mm () * 25.4 ;
	}
	*a_xdpi = thiz->xdpi ;
	return GDI_OK ;
}


static enum SXGdiStatus
get_ydpi (SXGdi *a_this,
	 gint *a_ydpi)
{
	enum SXGdiStatus result = GDI_OK ;
	struct SXGtkDiPrivate *thiz = NULL ;

	g_return_val_if_fail (a_this && sx_gtk_di_is_a (a_this),
			      GDI_BAD_PARAM_ERROR) ;

	result = sx_gdi_get_private_data (a_this, (gpointer)&thiz) ;
	g_return_val_if_fail (result == GDI_OK, GDI_BAD_PARAM_ERROR) ;

	if (!thiz->ydpi) {
		thiz->ydpi = gdk_screen_height () / 
			gdk_screen_height_mm () * 25.4 ;
	}
	*a_ydpi = thiz->ydpi ;
	return GDI_OK ;
}

static PangoWeight 
cr_font_weight_to_pango_font_weight (enum CRFontWeight a_weight)
{
        PangoWeight pgo_weight = PANGO_WEIGHT_NORMAL;

        switch (a_weight) {
        case FONT_WEIGHT_NORMAL:
                pgo_weight = PANGO_WEIGHT_NORMAL;
                break;
        case FONT_WEIGHT_BOLD:
                pgo_weight = PANGO_WEIGHT_BOLD;
                break;
        case FONT_WEIGHT_BOLDER:
                cr_utils_trace_info
                        ("font-weight: bolder should not be handled here.");
                break;
        case FONT_WEIGHT_LIGHTER:
                cr_utils_trace_info
                        ("font-weight: lighter is not supported yet");
                break;
        case FONT_WEIGHT_100:
                pgo_weight = 100;
                break;
        case FONT_WEIGHT_200:
                pgo_weight = 200;
                break;
        case FONT_WEIGHT_300:
                pgo_weight = 300;
                break;
        case FONT_WEIGHT_400:
                pgo_weight = 400;
                break;
        case FONT_WEIGHT_500:
                pgo_weight = 500;
                break;
        case FONT_WEIGHT_600:
                pgo_weight = 600;
                break;
        case FONT_WEIGHT_700:
                pgo_weight = 700;
                break;
        case FONT_WEIGHT_800:
                pgo_weight = 800;
                break;
        case FONT_WEIGHT_900:
                pgo_weight = 900;
                break;
        case FONT_WEIGHT_INHERIT:
                cr_utils_trace_info
                        ("font-weight: inherit should not be handled here.");
                break;
        default:
                cr_utils_trace_info ("unknown property value");
                break;
        }
        return pgo_weight ;
}


static PangoWeight 
get_bolder_font_for_pango (enum CRFontWeight a_weight)
{
        enum CRFontWeight bolder_font ;
        PangoWeight pgo_weight ;
        bolder_font = cr_font_weight_get_bolder (a_weight) ;
        pgo_weight = cr_font_weight_to_pango_font_weight (bolder_font) ;
        return pgo_weight ;
}

/**
 *TODO: resolve the case when we have
 *sizes like 2em.
 *This may mean that we need to split
 *font size in sv/cv/av too ... 
 *(which would be logical)
 */
static enum CRStatus
style_to_pango_font_attributes_list (CRStyle * a_style,
                                     PangoAttrList * a_pgo_attrs,
                                     gulong a_text_len)
{
        enum CRStatus status = CR_OK;
        PangoAttribute *pgo_attr = NULL;
        PangoFontDescription *pgo_font_desc = NULL;
        PangoStyle pgo_style = PANGO_STYLE_NORMAL;
        guchar *font_family = NULL;
        PangoWeight pgo_weight = PANGO_WEIGHT_NORMAL;

        g_return_val_if_fail (a_pgo_attrs && a_style, 
                              CR_BAD_PARAM_ERROR);

        pgo_font_desc = pango_font_description_new ();
        if (!pgo_font_desc) {
                cr_utils_trace_info ("Could not instanciate "
                                     "pango font description");
                return CR_ERROR;
        }

        /*set font size */
        switch (a_style->font_size.cv.type) {
        case PREDEFINED_ABSOLUTE_FONT_SIZE:
                if (!(a_style->font_size.cv.value.predefined
                      < NB_PREDEFINED_ABSOLUTE_FONT_SIZES)) {
                        status = CR_OUT_OF_BOUNDS_ERROR;
                        goto cleanup;
                }
                pango_font_description_set_size
                        (pgo_font_desc,
                         gv_predefined_abs_font_size_tab
                         [a_style->font_size.cv.value.predefined]
                         * PANGO_SCALE);
                break;

        case ABSOLUTE_FONT_SIZE:
                pango_font_description_set_size
                        (pgo_font_desc,
                         a_style->font_size.cv.value.absolute.val
                         * PANGO_SCALE);
                break;

        case RELATIVE_FONT_SIZE:
                cr_utils_trace_info
                        ("relative font size are not supported "
                         "yes");

                break;

        case INHERITED_FONT_SIZE:
                cr_utils_trace_info
                        ("inherited font size are not supported "
                         "yes");
                break;
        }

        /*set font family */
        if (a_style->font_family)
                font_family = cr_font_family_to_string (a_style->font_family,
                                                        TRUE);
        if (font_family) {
                pango_font_description_set_family (pgo_font_desc,
                                                   font_family);
        }

        /*set style */
        switch (a_style->font_style) {
        case FONT_STYLE_NORMAL:
                pgo_style = PANGO_STYLE_NORMAL;
                break;

        case FONT_STYLE_ITALIC:
                pgo_style = PANGO_STYLE_ITALIC;
                break;

        case FONT_STYLE_OBLIQUE:
                pgo_style = PANGO_STYLE_OBLIQUE;
                break;

        case FONT_STYLE_INHERIT:
                cr_utils_trace_info ("font-style: inherit not supported yet");
                break;

        default:
                cr_utils_trace_info ("unknown font-sytle property value");
                break;
        }

        pango_font_description_set_style (pgo_font_desc, pgo_style);

        /*set font weight */
        switch (a_style->font_weight) {
        case FONT_WEIGHT_NORMAL:
                pgo_weight = PANGO_WEIGHT_NORMAL;
                break;

        case FONT_WEIGHT_BOLD:
                pgo_weight = PANGO_WEIGHT_BOLD;
                break;

        case FONT_WEIGHT_BOLDER:
                pgo_weight = get_bolder_font_for_pango 
                        (a_style->parent_style->font_weight) ;
                break;

        case FONT_WEIGHT_LIGHTER:
                cr_utils_trace_info
                        ("font-weight: lighter is not supported yet");
                break;

        case FONT_WEIGHT_100:
                pgo_weight = 100;
                break;

        case FONT_WEIGHT_200:
                pgo_weight = 200;
                break;

        case FONT_WEIGHT_300:
                pgo_weight = 300;
                break;

        case FONT_WEIGHT_400:
                pgo_weight = 400;
                break;

        case FONT_WEIGHT_500:
                pgo_weight = 500;
                break;

        case FONT_WEIGHT_600:
                pgo_weight = 600;
                break;

        case FONT_WEIGHT_700:
                pgo_weight = 700;
                break;

        case FONT_WEIGHT_800:
                pgo_weight = 800;
                break;

        case FONT_WEIGHT_900:
                pgo_weight = 900;
                break;

        case FONT_WEIGHT_INHERIT:
                cr_utils_trace_info
                        ("font-weight: inherit is not supported yet.");
                break;

        default:
                cr_utils_trace_info ("unknown property value");
                break;
        }

        pango_font_description_set_weight (pgo_font_desc, pgo_weight);

        pgo_attr = pango_attr_font_desc_new (pgo_font_desc);
        if (!pgo_attr) {
                status = CR_INSTANCIATION_FAILED_ERROR;
                goto cleanup;
        }
        pgo_attr->start_index = 0;
        pgo_attr->end_index = a_text_len;
        pango_attr_list_change (a_pgo_attrs, pgo_attr);
        pgo_attr = NULL;

        /* Set font color */
        if (!a_style->rgb_props[RGB_PROP_COLOR].cv.is_percentage) {
                pgo_attr =
                        pango_attr_foreground_new (a_style->
                                                   rgb_props[RGB_PROP_COLOR].
                                                   cv.red << 8,
                                                   a_style->
                                                   rgb_props[RGB_PROP_COLOR].
                                                   cv.green << 8,
                                                   a_style->
                                                   rgb_props[RGB_PROP_COLOR].
                                                   cv.blue << 8);
        } else {
                guint16 red,
                  green,
                  blue;

                red = a_style->rgb_props[RGB_PROP_COLOR].cv.red;
                red = red * 0xFFFF / 100;
                green = a_style->rgb_props[RGB_PROP_COLOR].cv.green;
                green = green * 0xFFFF / 100;
                blue = a_style->rgb_props[RGB_PROP_COLOR].cv.blue;
                blue = blue * 0xFFFF / 100;
                pgo_attr = pango_attr_foreground_new (red, green, blue);
        }
        if (!pgo_attr) {
                status = CR_INSTANCIATION_FAILED_ERROR;
                goto cleanup;
        }
        pgo_attr->start_index = 0;
        pgo_attr->end_index = a_text_len;
        pango_attr_list_change (a_pgo_attrs, pgo_attr);
        pgo_attr = NULL;

 cleanup:
        if (pgo_attr) {
                pango_attribute_destroy (pgo_attr);
                pgo_attr = NULL;
        }
        if (pgo_font_desc) {
                pango_font_description_free (pgo_font_desc);
                pgo_font_desc = NULL;
        }

        return status;
}

static enum SXGdiStatus
layout_text_in_box (SXGdi *a_this, SXBox *a_text_box)
{
	enum SXGdiStatus result = GDI_OK ;
	struct SXGtkDiPrivate *thiz = NULL ;
        enum CRStatus status = CR_OK ;
	GtkWidget *label = NULL;
        PangoLayout *pgo_layout = NULL;
        PangoAttrList *pgo_attr_list = NULL;
        glong wrap_width = 0;

	g_return_val_if_fail (a_this 
			      && sx_gtk_di_is_a (a_this),
			      GDI_BAD_PARAM_ERROR) ;

	result = sx_gdi_get_private_data (a_this, (gpointer)&thiz) ;
	g_return_val_if_fail (result == GDI_OK && thiz,
			      GDI_BAD_PARAM_ERROR) ;
	
	g_return_val_if_fail (a_text_box
                              && a_text_box->content
                              && (a_text_box->content->type
                                  == TEXT_CONTENT_TYPE)
                              && a_text_box->content->u.text,
                              CR_BAD_PARAM_ERROR);
	if (! ((a_text_box->parent->inner_edge.max_width
                + a_text_box->parent->inner_edge.x)
               >= a_text_box->inner_edge.x)) {
                g_return_val_if_fail ((a_text_box->parent->inner_edge.max_width
                                       + a_text_box->parent->inner_edge.x)
                                      >= a_text_box->inner_edge.x,
                                      CR_BAD_PARAM_ERROR);
        }

        if (!a_text_box->content->content_cache) {
                a_text_box->content->content_cache = gtk_label_new (NULL);
                g_return_val_if_fail (a_text_box->content->content_cache,
                                      CR_ERROR);
        }

        label = a_text_box->content->content_cache;
        g_return_val_if_fail (GTK_IS_LABEL (label), CR_ERROR);

        gtk_label_set_text (GTK_LABEL (label), a_text_box->content->u.text);
        gtk_misc_set_alignment (GTK_MISC (label), 0, 0);
        gtk_misc_set_padding (GTK_MISC (label), 0, 0);
        gtk_label_set_use_markup (GTK_LABEL (label), FALSE);
        gtk_label_set_use_underline (GTK_LABEL (label), FALSE);
        pgo_layout = gtk_label_get_layout (GTK_LABEL (label));

        wrap_width = a_text_box->inner_edge.max_width;

        gtk_label_set_line_wrap (GTK_LABEL (label), TRUE);
        gtk_widget_set_size_request (label, wrap_width, -1);
	/*
         *set the font description attributes.
         */

        pgo_attr_list = pango_attr_list_new ();
        g_return_val_if_fail (pgo_attr_list, CR_ERROR);

        status = style_to_pango_font_attributes_list
                (a_text_box->style, pgo_attr_list,
                 strlen (a_text_box->content->u.text));

        gtk_label_set_attributes (GTK_LABEL (label), pgo_attr_list);

        return status;	
}

static enum SXGdiStatus
get_text_layout_pixel_extents (SXGdi *a_this,
			       SXBox *a_box,
			       struct SXGdiRectangle *a_rect)
{
	enum SXGdiStatus result = GDI_OK ;
	struct SXGtkDiPrivate *thiz = NULL ;

        GtkWidget *label = NULL;
        PangoLayout *pgo_layout = NULL;
        PangoRectangle ink_rect = { 0 }, logical_rect = {0};

	g_return_val_if_fail (a_this 
                              && sx_gtk_di_is_a (a_this)
                              && a_rect,
                              GDI_BAD_PARAM_ERROR) ;

	result = sx_gdi_get_private_data (a_this, (gpointer)&thiz) ;
	g_return_val_if_fail (result == GDI_OK, GDI_BAD_PARAM_ERROR) ;
	
	g_return_val_if_fail (a_box->content->content_cache,
                              GDI_BAD_PARAM_ERROR);

        label = a_box->content->content_cache;
	        pgo_layout = gtk_label_get_layout (GTK_LABEL (label));

        pango_layout_get_pixel_extents (pgo_layout, &ink_rect, &logical_rect);
	a_rect->x = logical_rect.x ;
	a_rect->y = logical_rect.y ;
	a_rect->width = logical_rect.width ;
	a_rect->height = logical_rect.height ;
	return GDI_OK ;
}

static enum SXGdiStatus 
nb_em_to_text_font_size (SXGdi *a_this,
                         CRStyle *a_style,
                         SXBox *a_box,
                         gdouble a_nb_em,
                         glong *a_font_size)
{
        enum CRStatus cr_status = CR_OK ;
        enum SXGdiStatus gdi_status = GDI_OK ;
        GtkLabel *label = NULL ;
        PangoAttrList * pango_attrs = NULL ;
        PangoAttrIterator *it = NULL ;
        PangoFontDescription *font_desc = NULL ;
        gint font_size = 0 ;

        g_return_val_if_fail (a_this
                              && a_box
                              && a_style
                              && a_font_size
                              && sx_gtk_di_is_a (a_this),
                              GDI_BAD_PARAM_ERROR) ;

        if (!a_nb_em) {
                *a_font_size = 0 ;
                return GDI_OK ;
        }
        pango_attrs = pango_attr_list_new () ;
        g_return_val_if_fail (pango_attrs, GDI_ERROR) ;
        cr_status = style_to_pango_font_attributes_list (a_style,
                                                         pango_attrs,
                                                         1);
        if (cr_status != CR_OK) {
                gdi_status = GDI_ERROR ;
                goto cleanup ;
        }
        label = GTK_LABEL (gtk_label_new ("foobar")) ;
        gtk_label_set_attributes (label, pango_attrs) ;

        it = pango_attr_list_get_iterator (pango_attrs) ;
        pango_attr_iterator_get_font (it, font_desc, 
                                      NULL, NULL) ;

        font_size = pango_font_description_get_size (font_desc) ;
        *a_font_size = font_size * a_nb_em ;
        gdi_status = GDI_OK ;

 cleanup:
        if (it) {
                pango_attr_iterator_destroy (it) ;
                it = NULL ;
        }
        if (pango_attrs) {
                pango_attr_list_unref (pango_attrs) ;
                pango_attrs = NULL ;
        }

        return gdi_status ;
}

static enum SXGdiStatus
predefined_absolute_font_size_to_point_size (SXGdi *a_this,
                                             enum CRPredefinedAbsoluteFontSize a_abs_size,
                                             gdouble *a_point_size)
{
        enum SXGdiStatus result = GDI_OK ;
        struct SXGtkDiPrivate *thiz = NULL ;

        g_return_val_if_fail (a_this && sx_gtk_di_is_a (a_this)
                              && a_point_size,
                              GDI_BAD_PARAM_ERROR) ;

        g_return_val_if_fail (a_abs_size >= FONT_SIZE_XX_SMALL
                              && a_abs_size < NB_PREDEFINED_ABSOLUTE_FONT_SIZES,
                              GDI_BAD_PARAM_ERROR) ;

        result = sx_gdi_get_private_data (a_this, (gpointer)&thiz) ;
	g_return_val_if_fail (result == GDI_OK && thiz,
			      GDI_BAD_PARAM_ERROR) ;
        
        *a_point_size =  gv_predefined_abs_font_size_tab[a_abs_size] ;
        return GDI_OK ;
}

static enum SXGdiStatus
init_interface_implementation (SXGdi *a_this)
{
	enum SXGdiStatus result = GDI_OK ;
	struct SXGtkDiPrivate *thiz = NULL ;

	g_return_val_if_fail (a_this 
			      && sx_gtk_di_is_a (a_this),
			      GDI_BAD_PARAM_ERROR) ;
	
	result = sx_gdi_get_private_data (a_this, (gpointer)&thiz) ;

	sx_gdi_set_handler_destroy_handler (a_this, 
                                            destroy_handler) ;

        sx_gdi_set_handler_get_xdpi (a_this, get_xdpi) ;

        sx_gdi_set_handler_get_ydpi (a_this, get_ydpi) ;

        sx_gdi_set_handler_layout_text_in_box (a_this, 
                                               layout_text_in_box) ;

        sx_gdi_set_handler_get_text_layout_pixel_extents 
                (a_this, get_text_layout_pixel_extents) ;

        sx_gdi_set_handler_nb_em_to_text_font_size 
                (a_this, nb_em_to_text_font_size) ;
        
        sx_gdi_set_handler_predefined_absolute_font_size_to_point_size 
                (a_this, predefined_absolute_font_size_to_point_size) ;

	return GDI_OK ;
}

/*********************
 *Public methods
 ********************/

SXGdi * 
sx_gtk_di_new (void)
{
	SXGdi *result = NULL ;
	struct SXGtkDiPrivate *priv_data = NULL ;
	
	priv_data = g_try_malloc (sizeof (struct SXGtkDiPrivate)) ;
	if (!priv_data) {
		cr_utils_trace_info ("Out of memory") ;
		goto error ;
	}
	memset (priv_data, 0, sizeof (struct SXGtkDiPrivate)) ;
	priv_data->magic = 0xdead0 ;
        priv_data->layout = GTK_LAYOUT (gtk_layout_new (NULL, NULL)) ;
        if (!priv_data->layout) {
                cr_utils_trace_info ("Coult not instanciate GtkLayout") ;
                goto error ;
        }
	result = sx_gdi_new (priv_data) ;
	if (!result) {
		cr_utils_trace_info ("Out of memory") ;
		goto error ;
	}
        init_interface_implementation (result) ;
	return result ;

 error:
	if (priv_data) {
		g_free (priv_data) ;
		priv_data = NULL ;
	}
	if (result) {
		g_free (result) ;
		result = NULL ;
	}
	return NULL ;
}

gboolean
sx_gtk_di_is_a (SXGdi *a_this)
{
	struct SXGtkDiPrivate *priv = NULL;
	enum SXGdiStatus result = GDI_OK ;

	g_return_val_if_fail (a_this, FALSE) ;

	result = sx_gdi_get_private_data (a_this, (gpointer)&priv) ;
	g_return_val_if_fail (result == GDI_OK, FALSE) ;
	return (priv->magic == 0xdead0) ;
}

