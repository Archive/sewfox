/* -*- Mode: C; indent-tabs-mode:nil; c-basic-offset: 8-*- */

/*
 * This file is part of the SEWFOX project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2.1 of the GNU Lesser General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 * 
 * Author: Dodji Seketeli.
 * See COPYRIGHTS file for copyright information.
 */

/*
 *$Id$
 */

#include <string.h>
#include "sx-box.h"

/**
 *@file
 *The definition file of the #SXBox class.
 */

static enum CRStatus
sx_box_construct (SXBox * a_this, CRStyle * a_style,
                    gboolean a_set_default_style);

static enum CRStatus
sx_box_edge_to_string (SXBoxEdge * a_this,
                         gulong a_nb_indent, GString ** a_string);

static enum SXBoxType
sx_box_guess_type (CRStyle * a_style);

/******************************
 *Private methods
 ******************************/

/**
 *Guess the type of a box from the 'position' rule
 *contained in its style data structure.
 *@param a_style the style data structure associated to the box.
 */
static enum SXBoxType
sx_box_guess_type (CRStyle * a_style)
{
        enum SXBoxType box_type = BOX_TYPE_INLINE;

        if (!a_style)
                return box_type;

        switch (a_style->display) {
        case DISPLAY_NONE:
                break;

        case DISPLAY_INLINE:
        case DISPLAY_MARKER:
                box_type = BOX_TYPE_INLINE;
                break;

        case DISPLAY_BLOCK:
        case DISPLAY_LIST_ITEM:
        case DISPLAY_TABLE:
        case DISPLAY_INLINE_TABLE:
        case DISPLAY_TABLE_ROW_GROUP:
        case DISPLAY_TABLE_HEADER_GROUP:
        case DISPLAY_TABLE_FOOTER_GROUP:
        case DISPLAY_TABLE_ROW:
        case DISPLAY_TABLE_COLUMN_GROUP:
        case DISPLAY_TABLE_COLUMN:
        case DISPLAY_TABLE_CELL:
        case DISPLAY_TABLE_CAPTION:
                box_type = BOX_TYPE_BLOCK;
                break;

        case DISPLAY_COMPACT:
                box_type = BOX_TYPE_COMPACT;
                break;

        case DISPLAY_RUN_IN:
                box_type = BOX_TYPE_RUN_IN;
                break;

        case DISPLAY_INHERIT:
                if (a_style->parent_style)
                        box_type = sx_box_guess_type (a_style->parent_style);
                break;

        default:
                box_type = BOX_TYPE_INLINE;
                break;
        }

        return box_type;
}

static enum CRStatus
sx_box_construct (SXBox * a_this, CRStyle * a_style,
                  gboolean a_set_default_style)
{
        CRStyle *style = a_style;

        if (!style && a_set_default_style == TRUE) {
                style = cr_style_new (FALSE);
                if (!style) {
                        cr_utils_trace_info ("Could not create style object");
                        cr_utils_trace_info ("System may be out of memory");
                        return CR_ERROR;
                }
        }
        if (a_set_default_style == TRUE) {
                sx_box_set_style (a_this, a_style);
        }

        return CR_OK;
}

static enum CRStatus
sx_box_edge_to_string (SXBoxEdge * a_this,
                       gulong a_nb_indent, GString ** a_string)
{
        GString *result = NULL;

        if (*a_string) {
                result = *a_string;
        } else {
                result = g_string_new (NULL);
                if (!result) {
                        cr_utils_trace_info ("Out of memory");
                        return CR_ERROR;
                }
        }

        cr_utils_dump_n_chars2 (' ', result, a_nb_indent);
        g_string_append_printf (result, "(%ld, %ld)\n",
                                (long int) a_this->x, (long int) a_this->y);
        cr_utils_dump_n_chars2 (' ', result, a_nb_indent);
        g_string_append_printf (result, "width: %ld\n",
                                (long int) a_this->width);

        cr_utils_dump_n_chars2 (' ', result, a_nb_indent);
        g_string_append_printf (result, "max-width: %ld\n",
                                (long int) a_this->max_width);

        cr_utils_dump_n_chars2 (' ', result, a_nb_indent);
        g_string_append_printf (result, "height: %ld\n",
                                (long int) a_this->height);
        cr_utils_dump_n_chars2 (' ', result, a_nb_indent);
        g_string_append_printf (result, "x_offset: %ld\n",
                                (long int) a_this->x_offset);
        cr_utils_dump_n_chars2 (' ', result, a_nb_indent);
        g_string_append_printf (result, "y_offset: %ld\n",
                                (long int) a_this->y_offset);

        return CR_OK;
}

/*******************************
 *Public methods
 *******************************/

/**
 *Instanciates a new #SXBoxData.
 *@param a_node the xml node to store in the box.
 *@return the newly built #SXBoxData, or null if an error arises.
 */
SXBoxData *
sx_box_data_new (xmlNode * a_node)
{
        SXBoxData *result = NULL;

        result = g_try_malloc (sizeof (SXBoxData));
        if (!result) {
                cr_utils_trace_info ("Out of memory");
                return NULL;
        }
        memset (result, 0, sizeof (SXBoxData));
        result->xml_node = a_node;
        return result;
}

/**
 *Destructor of #SXBoxData.
 *@param a_this the current instance 
 *of #SXBoxData to be destroyed.
 */
void
sx_box_data_destroy (SXBoxData * a_this)
{
        if (!a_this)
                return;

        g_free (a_this);
}

/**
 *Instanciates a new #SXBoxContent and set the
 *content to text content.
 *@param a_text the text content.
 */
SXBoxContent *
sx_box_content_new_from_text (guchar * a_text)
{
        SXBoxContent *result = NULL;

        g_return_val_if_fail (a_text, NULL);

        result = g_try_malloc (sizeof (SXBoxContent));
        if (!result) {
                cr_utils_trace_info ("Out of memory");
                return NULL;
        }
        memset (result, 0, sizeof (SXBoxContent));
        result->u.text = g_strdup (a_text);
        result->type = TEXT_CONTENT_TYPE;
        return result;
}

/**
 *Destructor of #SXBoxContent.
 *@param a_this the current instance of #SXBoxContent
 *to be destroyed.
 */
void
sx_box_content_destroy (SXBoxContent * a_this)
{
        if (!a_this)
                return;

        switch (a_this->type) {
        case TEXT_CONTENT_TYPE:
                if (a_this->u.text) {
                        g_free (a_this->u.text);
                        a_this->u.text = NULL;
                }
                break;

        default:
                cr_utils_trace_info ("Unrecognized box content type");
                cr_utils_trace_info ("This seems to be a mem leak");
                break;
        }

        g_free (a_this);
        return;
}

enum CRStatus
sx_box_set_style (SXBox * a_this, CRStyle * a_style)
{
        g_return_val_if_fail (a_this, CR_BAD_PARAM_ERROR);

        if (a_this->style) {
                cr_style_unref (a_this->style);
                a_this->style = NULL;
        }
        a_this->style = a_style;
        if (a_this->style) {
                cr_style_ref (a_this->style);
        }
        a_this->type = sx_box_guess_type (a_this->style);
        return CR_OK;
}

/**
 *Creates a new box model.
 *This box model contains an empty box tree.
 *Box tree may be added by calling sx_box_append_child().
 *@param a_doc the xml document associated to this box model.
 *@return the newly built instance of #SXBoxModel, or NULL if an
 *error arises.
 */
SXBoxModel *
sx_box_model_new (xmlDoc *a_doc)
{
        SXBoxModel *result = NULL;
        SXBoxData *box_data = NULL ;
        result = g_try_malloc (sizeof (SXBoxModel));
        if (!result) {
                cr_utils_trace_info ("Out of memory");
                return NULL;
        }

        memset (result, 0, sizeof (SXBoxModel));

        sx_box_construct (&result->box, NULL, FALSE);

        ((SXBox *) result)->type = BOX_TYPE_BOX_MODEL;
        ((SXBox *) result)->box_model = result;
        result->norm_ws = TRUE ;
        if (a_doc) {
                box_data = sx_box_data_new ((xmlNode*)a_doc) ;
                if (!box_data) {
                        cr_utils_trace_info 
                                ("Could not instanciate SXBoxData") ;
                        g_free (result) ;
                        return NULL ;
                }
                ((SXBox*)result)->box_data = box_data ;
        }
        ((SXBox*)result)->box_model = result ;
        return result;
}

void
sx_box_model_destroy (SXBoxModel * a_this)
{
        g_return_if_fail (a_this);

        sx_box_destroy (&a_this->box);

        g_free (a_this);
}

void
sx_box_model_ref (SXBoxModel * a_this)
{
        if (a_this && a_this->ref_count) {
                a_this->ref_count++;
        }
}

gboolean
sx_box_model_unref (SXBoxModel * a_this)
{
        if (a_this && a_this->ref_count) {
                a_this->ref_count--;
        }

        if (a_this && a_this->ref_count == 0) {
                sx_box_model_destroy (a_this);
                return TRUE;
        }

        return FALSE;
}

/**
 *Instanciates a new box.
 *Everything is initialized to zero in it.
 *@return the newly created box.
 */
SXBox *
sx_box_new (CRStyle * a_style, gboolean a_default_style)
{
        SXBox *result = NULL;

        result = g_try_malloc (sizeof (SXBox));
        if (!result) {
                cr_utils_trace_info ("Out of memory");
                goto error;
        }
        memset (result, 0, sizeof (SXBox));

        if (sx_box_construct (result, a_style, a_default_style) == CR_OK)
                return result;

 error:
        if (result) {
                sx_box_destroy (result);
                result = NULL;
        }

        return NULL;
}

/**
 *Appends a child box to at the end of the current box's children.
 *@param a_this the current box.
 *@param a_to_append, the box to append.
 *@retrurn CR_OK upon successfull completion, an error code otherwise.
 */
enum CRStatus
sx_box_append_child (SXBox * a_this, SXBox * a_to_append)
{
        SXBox *cur = NULL;

        g_return_val_if_fail (a_this
                              && a_this->box_model
                              && a_to_append, CR_BAD_PARAM_ERROR);

        if (!a_this->children) {
                a_this->children = a_to_append;
                a_to_append->prev = NULL;
                a_to_append->parent = a_this;
                a_to_append->box_model = a_this->box_model;
                return CR_OK;
        }

        for (cur = a_this->children; cur && cur->next; cur = cur->next) ;

        cur->next = a_to_append;
        a_to_append->prev = cur;
        a_to_append->parent = a_this;
        a_to_append->box_model = a_this->box_model;

        return CR_OK;
}

/**
 *Inserts a sibling box between two adjacent sibling nodes.
 *@param a_prev the box after which we have to insert a new one.
 *@param a_next the box before which we have to insert a new one.
 *@param a_to_insert the node to insert.
 */
enum CRStatus
sx_box_insert_sibling (SXBox * a_prev, SXBox * a_next, SXBox * a_to_insert)
{
        g_return_val_if_fail (a_prev && a_prev->parent
                              && a_next && a_prev->next == a_next
                              && a_next->parent == a_prev->parent
                              && a_prev->box_model
                              && a_prev->box_model == a_next->box_model
                              && a_to_insert
                              && a_to_insert->parent != a_prev->parent,
                              CR_BAD_PARAM_ERROR);

        a_prev->next = a_to_insert;
        a_to_insert->prev = a_prev;
        a_to_insert->next = a_next;
        a_next->prev = a_to_insert;
        a_to_insert->parent = a_prev->parent;
        a_to_insert->box_model = a_prev->box_model;

        return CR_OK;
}

/**
 *Returns the root xml element node associated to the
 *box model this box belongs to.
 *@param a_box the box to consider.
 *@return the root xml node.
 */
xmlNode *
sx_box_get_xml_root_element_node (SXBox *a_this)
{
        g_return_val_if_fail (a_this, NULL) ;
        g_return_val_if_fail (a_this->box_model, NULL) ;

        if (a_this->box_model->box.box_data) {
                return a_this->box_model->box.box_data->xml_node ;
        }
        cr_utils_trace_info ("Could not get xml root element node") ;
        return NULL ;
}


/**
 *@param a_this the box to consider.
 *@return TRUE if a_this is a root box, false otherwise
 */
gboolean
sx_box_is_root_box (SXBox *a_this)
{
        g_return_val_if_fail (a_this, FALSE) ;

        g_return_val_if_fail (a_this->box_model, FALSE) ;
        
        if (((SXBox*)a_this->box_model)->children == a_this)
                return TRUE ;
        return FALSE ;
}

/**
 *This is for debug purposes ...
 *Gives a string representation of the box tree.
 *@return the build string of NULL in case of an error.
 */
enum CRStatus
sx_box_to_string (SXBox * a_this, gulong a_nb_indent, GString ** a_string)
{
        GString *result = NULL;
        SXBox *cur_box = NULL;

        g_return_val_if_fail (a_this && a_string, CR_BAD_PARAM_ERROR);

        if (*a_string) {
                result = *a_string;
        } else {
                result = g_string_new (NULL);
                if (!result) {
                        cr_utils_trace_info ("Out of memory");
                        return CR_ERROR;
                }
                *a_string = result;
        }

        for (cur_box = a_this; cur_box; cur_box = cur_box->next) {
                if (cur_box->prev || cur_box->parent)
                        g_string_append_printf (result, "\n\n");

                cr_utils_dump_n_chars2 (' ', result, a_nb_indent);

                switch (cur_box->type) {
                case BOX_TYPE_BLOCK:
                        g_string_append_printf (result, "BLOCK");
                        break;

                case BOX_TYPE_ANONYMOUS_BLOCK:
                        g_string_append_printf (result, "ANONYMOUS BLOCK");
                        break;

                case BOX_TYPE_INLINE:
                        g_string_append_printf (result, "INLINE");
                        break;

                case BOX_TYPE_ANONYMOUS_INLINE:
                        g_string_append_printf (result, "ANONYMOUS INLINE");
                        break;

                case BOX_TYPE_COMPACT:
                        g_string_append_printf (result, "COMPACT");
                        break;

                case BOX_TYPE_RUN_IN:
                        g_string_append_printf (result, "RUN IN");
                        break;

                case BOX_TYPE_BOX_MODEL:
                        g_string_append_printf (result, "Root");
                        break;

                default:
                        g_string_append_printf (result, "UNKNOWN");
                        break;
                }

                g_string_append_printf (result, " box\n");
                cr_utils_dump_n_chars2 (' ', result, a_nb_indent);
                g_string_append_printf (result, "{");

                if (cur_box->box_data && cur_box->box_data->xml_node) {
                        switch (cur_box->box_data->xml_node->type) {
                        case XML_ELEMENT_NODE:
                                cr_utils_dump_n_chars2
                                        (' ', result, a_nb_indent);
                                g_string_append_printf
                                        (result, "xml-node-name: %s\n",
                                         cur_box->box_data->xml_node->name);
                                break;

                        case XML_TEXT_NODE:
                                cr_utils_dump_n_chars2
                                        (' ', result, a_nb_indent);
                                g_string_append_printf
                                        (result, "xml-text-node\n");
                                break;

                        default:
                                break;
                        }
                }
                cr_utils_dump_n_chars2 (' ', result, a_nb_indent + 2);
                g_string_append_printf (result, "/*****Style begin*****/\n");
                if (cur_box->style) {
                        cr_style_to_string (cur_box->style, &result,
                                            a_nb_indent + 2);
                } else {
                        cr_utils_dump_n_chars2 (' ', result, a_nb_indent + 2);
                        g_string_append_printf (result, "NULL");
                }
                g_string_append_printf (result, "\n");

                cr_utils_dump_n_chars2 (' ', result, a_nb_indent + 2);
                g_string_append_printf (result, "/*****Style begin*****/\n");

                cr_utils_dump_n_chars2 (' ', result, a_nb_indent + 2);
                g_string_append_printf (result, "/*****%s begin*****/\n",
                                        "outer_edge");
                sx_box_edge_to_string (&cur_box->outer_edge,
                                       a_nb_indent + 2, &result);
                cr_utils_dump_n_chars2 (' ', result, a_nb_indent + 2);
                g_string_append_printf (result, "/*****%s end*****/\n",
                                        "outer_edge");

                cr_utils_dump_n_chars2 (' ', result, a_nb_indent + 2);
                g_string_append_printf (result, "/*****%s begin*****/\n",
                                        "border_edge");
                sx_box_edge_to_string (&cur_box->border_edge,
                                       a_nb_indent + 2, &result);
                cr_utils_dump_n_chars2 (' ', result, a_nb_indent + 2);
                g_string_append_printf (result, "/*****%s end*****/\n",
                                        "border_edge");

                cr_utils_dump_n_chars2 (' ', result, a_nb_indent + 2);
                g_string_append_printf (result, "/*****%s begin*****/\n",
                                        "padding_edge");
                sx_box_edge_to_string (&cur_box->padding_edge,
                                       a_nb_indent + 2, &result);
                cr_utils_dump_n_chars2 (' ', result, a_nb_indent + 2);
                g_string_append_printf (result, "/*****%s end*****/\n",
                                        "padding_edge");

                cr_utils_dump_n_chars2 (' ', result, a_nb_indent + 2);
                g_string_append_printf (result, "/*****%s begin*****/\n",
                                        "inner_edge");
                sx_box_edge_to_string (&cur_box->inner_edge,
                                       a_nb_indent + 2, &result);
                cr_utils_dump_n_chars2 (' ', result, a_nb_indent + 2);
                g_string_append_printf (result, "/*****%s end*****/\n",
                                        "inner_edge");

                if (cur_box->children) {
                        g_string_append_printf (result, "\n");
                        sx_box_to_string (cur_box->children,
                                          a_nb_indent + 2, &result);
                }

                g_string_append_printf (result, "\n");
                cr_utils_dump_n_chars2 (' ', result, a_nb_indent);
                g_string_append_printf (result, "}\n");
        }

        return CR_OK;
}

enum CRStatus
sx_box_dump_to_file (SXBox * a_this, gulong a_nb_indent, FILE * a_filep)
{
        GString *str = NULL;
        enum CRStatus status = CR_OK;

        g_return_val_if_fail (a_this && a_filep, CR_BAD_PARAM_ERROR);

        status = sx_box_to_string (a_this, a_nb_indent, &str);

        if (status != CR_OK) {
                cr_utils_trace_info ("An error occured "
                                     "during in memory serialisation");
                goto cleanup;
        }

        if (!str || !str->str) {
                cr_utils_trace_info ("Error: Box could not be serialised");
                goto cleanup;
        }

        if (!fwrite (str->str, 1, str->len, a_filep)) {
                cr_utils_trace_info ("An error occured during"
                                     "serialisation into file");
                status = CR_ERROR;
                goto cleanup;
        }

        status = CR_OK;

      cleanup:

        if (str) {
                g_string_free (str, TRUE);
                str = NULL;
        }
        return status;
}

/**
 *Increments the reference count of
 *the current instance of #SXBox.
 */
enum CRStatus
sx_box_ref (SXBox * a_this)
{
        g_return_val_if_fail (a_this, CR_BAD_PARAM_ERROR);

        a_this->ref_count++;

        return TRUE;
}

/**
 *Decrements the current instance's ref count.
 *If the ref count reaches zero, the instance is destroyed.
 *@param a_this the current instance.
 *@return TRUE if the ref count reached zero and the instance has been
 *destroyed, FALSE otherwise.
 */
gboolean
sx_box_unref (SXBox * a_this)
{
        g_return_val_if_fail (a_this, FALSE);

        if (a_this->ref_count)
                a_this->ref_count--;

        if (a_this->ref_count == 0) {
                sx_box_destroy (a_this);
                return TRUE;
        }

        return FALSE;
}

/**
 *Destructor of #SXBox.
 *recursively destroys all
 *the children nodes of the current node.
 *@param a_this the current box to destroy.
 */
void
sx_box_destroy (SXBox * a_this)
{
        SXBox *cur_box = NULL;

        g_return_if_fail (a_this);

        for (cur_box = a_this; cur_box && cur_box->next;
             cur_box = cur_box->next) ;

        for (; cur_box; cur_box = cur_box->prev) {
                if (cur_box->content) {
                        sx_box_content_destroy (cur_box->content);
                        cur_box->content = NULL;
                }

                if (cur_box->style) {
                        cr_style_unref (cur_box->style);
                        cur_box->style = NULL;
                }

                if (cur_box->next) {
                        g_free (cur_box->next);
                        cur_box->next = NULL;
                }

                if (cur_box->children) {
                        sx_box_destroy (cur_box->children);
                        g_free (cur_box->children);
                        cur_box->children = NULL;
                }
        }
}
