/* -*- Mode: C; indent-tabs-mode:nil; c-basic-offset: 8-*- */

/*
 * This file is part of the SEWFOX project
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2.1 of 
 * the GNU Lesser General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the 
 * GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * Author: Dodji Seketeli
 * See the COPYRIGHTS file for copyrights information
 */

#include <string.h>
#include <libcroco/cr-om-parser.h>
#include <libxml/tree.h>
#include <libxml/HTMLparser.h>
#include "sx-gtk-di.h"
#include "sx-box-view.h"
#include "sx-lay-eng.h"

#define PRIVATE(a_this) ((a_this)->priv)

struct _SXBoxViewPriv {
        SXBoxModel *box_model;
        /*
         *The default graphical context
         *Function willing to modify this gc
         *should save it firts, then modify it,
         *draw what they have to draw and then restore it !!
         */
        GdkGC *gc;

        SXLayEng *layeng;

        /**
         * a boolean used by some drawing functions.
         *greping PRIVATE (a_this)->draw should tell you who uses it :)
         */
        gboolean draw;
        gboolean use_ua_sheet ;
        gboolean dispose_has_run;
};

static GtkLayoutClass *gv_parent_class = NULL;

static void sx_box_view_class_init (SXBoxViewClass * a_klass);

static void sx_box_view_init (SXBoxView * a_this);

static enum CRStatus draw_box (SXBoxView * a_this, 
                               SXBox * a_box, 
                               gboolean put_widgets);

static enum CRStatus draw_borders (SXBoxView * a_bv, 
                                   SXBox * a_box);

static enum CRStatus draw_paddings (SXBoxView * a_bv, 
                                    SXBox * a_box);

static enum CRStatus draw_inner_box (SXBoxView * a_bv, 
                                     SXBox * a_box, 
                                     gboolean put_widgets);

static enum CRStatus set_border_line_attrs (SXBoxView * a_this,
                                            SXBox * a_box, 
                                            enum CRBorderStyleProp a_style_prop);

static enum CRStatus set_color (SXBoxView * a_this, 
                                CRRgb * a_rgb_color, 
                                gboolean a_foreground);

static void
sx_box_view_class_init (SXBoxViewClass * a_klass)
{
        GObjectClass *gobject_class = G_OBJECT_CLASS (a_klass);

        gv_parent_class = g_type_class_peek_parent (a_klass);

        g_return_if_fail (gv_parent_class);
        g_return_if_fail (gobject_class);
        gobject_class->dispose = sx_box_view_dispose;
        gobject_class->finalize = sx_box_view_finalize;
}

static void
sx_box_view_init (SXBoxView * a_this)
{
        g_return_if_fail (a_this);

        PRIVATE (a_this) = g_try_malloc (sizeof (SXBoxView));
        if (!PRIVATE (a_this)) {
                cr_utils_trace_info ("Out of memory");
                return;
        }
        memset (PRIVATE (a_this), 0, sizeof (SXBoxView));
}

static gboolean
map_event_cb (GtkWidget * a_this, gpointer a_user_data)
{
        g_return_val_if_fail (a_this && GTK_IS_LAYOUT (a_this)
                              && SX_IS_BOX_VIEW (a_this), CR_BAD_PARAM_ERROR);

        g_return_val_if_fail
                (PRIVATE (SX_BOX_VIEW (a_this))->box_model
                 && ((SXBox *) PRIVATE (SX_BOX_VIEW (a_this))->box_model)->
                 children, FALSE);
        sx_box_view_layout (SX_BOX_VIEW (a_this));

        draw_box (SX_BOX_VIEW (a_this),
                  ((SXBox *) PRIVATE (SX_BOX_VIEW (a_this))->box_model)->
                  children, TRUE);

        return FALSE;
}

static gboolean
expose_event_cb (GtkWidget * a_this,
                 GdkEventExpose * a_event, gpointer a_user_data)
{
        g_return_val_if_fail
                (PRIVATE (SX_BOX_VIEW (a_this))->box_model
                 && ((SXBox *) PRIVATE (SX_BOX_VIEW (a_this))->box_model)->
                 children, FALSE);

        draw_box (SX_BOX_VIEW (a_this),
                  ((SXBox *) PRIVATE (SX_BOX_VIEW (a_this))->box_model)->
                  children, FALSE);
        return FALSE;
}

static enum CRStatus
set_border_line_attrs (SXBoxView * a_this,
                       SXBox * a_box, 
                       enum CRBorderStyleProp a_style_prop)
{
        enum CRNumProp border_width_dir;
        GdkGCValues gc_values;

        g_return_val_if_fail (a_this && PRIVATE (a_this)
                              && a_box
                              && a_style_prop < NB_BORDER_STYLE_PROPS,
                              CR_BAD_PARAM_ERROR);

        memset (&gc_values, 0, sizeof (GdkGCValues));

        gdk_gc_get_values (PRIVATE (a_this)->gc, &gc_values);

        switch (a_style_prop) {
        case BORDER_STYLE_PROP_TOP:
                border_width_dir = NUM_PROP_BORDER_TOP;
                set_color
                        (a_this,
                         &a_box->style->rgb_props[RGB_PROP_BORDER_TOP_COLOR].
                         cv, TRUE /*foreground */ );
                break;

        case BORDER_STYLE_PROP_RIGHT:
                border_width_dir = NUM_PROP_BORDER_RIGHT;
                set_color (a_this,
                           &a_box->style->
                           rgb_props[RGB_PROP_BORDER_RIGHT_COLOR].cv,
                           TRUE /*foreground */ );
                break;

        case BORDER_STYLE_PROP_BOTTOM:
                border_width_dir = NUM_PROP_BORDER_BOTTOM;
                set_color (a_this,
                           &a_box->style->
                           rgb_props[RGB_PROP_BORDER_BOTTOM_COLOR].cv,
                           TRUE /*foreground */ );
                break;

        case BORDER_STYLE_PROP_LEFT:
                border_width_dir = NUM_PROP_BORDER_LEFT;
                set_color (a_this,
                           &a_box->style->
                           rgb_props[RGB_PROP_BORDER_LEFT_COLOR].cv,
                           TRUE /*foreground */ );
                break;

        default:
                cr_utils_trace_info ("Bad value of enum CRBorderStyleProp "
                                     "given in parameter");
                return CR_BAD_PARAM_ERROR;

        }

        switch (a_box->style->border_style_props[a_style_prop]) {
        case BORDER_STYLE_NONE:
        case BORDER_STYLE_HIDDEN:
                PRIVATE (a_this)->draw = FALSE;
                break;

        case BORDER_STYLE_DOTTED:
                gdk_gc_set_line_attributes
                        (PRIVATE (a_this)->gc,
                         a_box->style->num_props[border_width_dir].cv.val,
                         GDK_LINE_ON_OFF_DASH,
                         gc_values.cap_style, gc_values.join_style);
                PRIVATE (a_this)->draw = TRUE;
                break;

        case BORDER_STYLE_DASHED:
                gdk_gc_set_line_attributes
                        (PRIVATE (a_this)->gc,
                         a_box->style->num_props[border_width_dir].cv.val,
                         GDK_LINE_ON_OFF_DASH,
                         gc_values.cap_style, gc_values.join_style);
                PRIVATE (a_this)->draw = TRUE;
                break;

        case BORDER_STYLE_SOLID:
        case BORDER_STYLE_DOUBLE:
        case BORDER_STYLE_GROOVE:
        case BORDER_STYLE_RIDGE:
        case BORDER_STYLE_INSET:
        case BORDER_STYLE_OUTSET:
                gdk_gc_set_line_attributes
                        (PRIVATE (a_this)->gc,
                         a_box->style->num_props[border_width_dir].cv.val,
                         GDK_LINE_SOLID,
                         gc_values.cap_style, gc_values.join_style);
                PRIVATE (a_this)->draw = TRUE;
                break;
        case BORDER_STYLE_INHERIT:
                cr_utils_trace_info 
                        ("found BORDER_STYLE_INHERIT: inheritance not resolved for this node.") ;
                break ;
        }

        return CR_OK;
}

static enum CRStatus
set_color (SXBoxView * a_this, CRRgb * a_rgb_color, gboolean a_foreground)
{
        GdkColor gdk_color = { 0 };
        g_return_val_if_fail (a_this && a_rgb_color, CR_BAD_PARAM_ERROR);

        gdk_color.red = (a_rgb_color->red << 8) | a_rgb_color->red;
        gdk_color.green = (a_rgb_color->green << 8) | a_rgb_color->green;
        gdk_color.blue = (a_rgb_color->blue << 8) | a_rgb_color->blue;

        gdk_rgb_find_color
                (gdk_drawable_get_colormap
                 (GDK_DRAWABLE (GTK_LAYOUT (a_this)->bin_window)),
                 &gdk_color);

        if (a_foreground == FALSE) {
                gdk_gc_set_background (PRIVATE (a_this)->gc, &gdk_color);
        } else {
                gdk_gc_set_foreground (PRIVATE (a_this)->gc, &gdk_color);
        }

        return CR_OK;
}

static enum CRStatus
draw_borders (SXBoxView * a_this, SXBox * a_box)
{
        GdkWindow *window = NULL;
        GtkWidget *widget = NULL;
        SXBox *box = NULL;

        gulong x0 = 0,
                y0 = 0,
                x1 = 0,
                y1 = 0;
        enum CRStatus status = CR_OK;

        g_return_val_if_fail (a_this && SX_IS_BOX_VIEW (a_this)
                              && a_box, CR_BAD_PARAM_ERROR);

        widget = GTK_WIDGET (a_this);
        window = GTK_LAYOUT (a_this)->bin_window;
        g_return_val_if_fail (window, CR_ERROR);

        box = a_box;
        g_return_val_if_fail (box, CR_ERROR);

        /*
         *Draw left border.
         */
        x0 = box->border_edge.x + (box->padding_edge.x -
                                   box->border_edge.x) / 2;
        /*x0 = box->border_edge.x ; */
        y0 = box->border_edge.y;
        x1 = x0;
        y1 = y0 + box->border_edge.height;
        status = set_border_line_attrs (a_this, a_box,
                                        BORDER_STYLE_PROP_LEFT);
        g_return_val_if_fail (status == CR_OK, status);

        if (PRIVATE (a_this)->draw == TRUE)
                gdk_draw_line (window, PRIVATE (a_this)->gc, x0, y0, x1, y1);

        /*
         *draw right border
         */
        x0 = box->padding_edge.x + box->padding_edge.width +
                (box->border_edge.x + box->border_edge.width
                 - box->padding_edge.x - box->padding_edge.width) / 2;
        y0 = box->border_edge.y;
        x1 = x0;
        /*y1 remains the same as y0 */
        status = set_border_line_attrs (a_this, a_box,
                                        BORDER_STYLE_PROP_RIGHT);
        g_return_val_if_fail (status == CR_OK, status);

        if (PRIVATE (a_this)->draw == TRUE)
                gdk_draw_line (window, PRIVATE (a_this)->gc, x0, y0, x1, y1);

        /*
         *draw top border.
         */
        x0 = box->border_edge.x;
        y0 = box->border_edge.y + (box->padding_edge.y -
                                   box->border_edge.y) / 2;
        /*y0 = box->border_edge.y ; */
        x1 = x0 + box->border_edge.width;
        y1 = y0;
        status = set_border_line_attrs (a_this, a_box, BORDER_STYLE_PROP_TOP);
        g_return_val_if_fail (status == CR_OK, status);

        if (PRIVATE (a_this)->draw == TRUE)
                gdk_draw_line (window, PRIVATE (a_this)->gc, x0, y0, x1, y1);

        /*
         *draw bottom border
         */
        /*x0 remains the same as previous x0 ; */

        y0 = box->padding_edge.y + box->padding_edge.height +
                (box->border_edge.y + box->border_edge.height
                 - box->padding_edge.y - box->padding_edge.height) / 2;

        /*y0 = box->padding_edge.y + box->padding_edge.height ; */
        x1 = x0 + box->border_edge.width;
        y1 = y0;
        status = set_border_line_attrs (a_this, a_box,
                                        BORDER_STYLE_PROP_BOTTOM);
        g_return_val_if_fail (status == CR_OK, status);

        if (PRIVATE (a_this)->draw == TRUE)
                gdk_draw_line (window, PRIVATE (a_this)->gc, x0, y0, x1, y1);

        PRIVATE (a_this)->draw = TRUE;

        return CR_OK;
}

static enum CRStatus
draw_paddings (SXBoxView * a_this, SXBox * a_box)
{
        GdkWindow *window = NULL;
        GtkWidget *widget = NULL;
        SXBox *box = NULL;

        g_return_val_if_fail (a_this && SX_IS_BOX_VIEW (a_this)
                              && a_box, CR_BAD_PARAM_ERROR);

        widget = GTK_WIDGET (a_this);
        window = GTK_LAYOUT (a_this)->bin_window;
        g_return_val_if_fail (window, CR_ERROR);

        box = a_box;
        g_return_val_if_fail (box, CR_ERROR);

        set_color (a_this,
                   &a_box->style->rgb_props[RGB_PROP_BACKGROUND_COLOR].cv,
                   TRUE /*foreground */ );

        gdk_draw_rectangle
                (window,
                 PRIVATE (a_this)->gc,
                 TRUE,
                 box->padding_edge.x, box->padding_edge.y,
                 box->padding_edge.width, box->padding_edge.height);

        return CR_OK;
}

static enum CRStatus
draw_inner_box (SXBoxView * a_this, SXBox * a_box, gboolean a_put_widgets)
{
        GtkWidget *widget = NULL,
                *label = NULL;
        SXBox *box = NULL;

        g_return_val_if_fail (a_this && SX_IS_BOX_VIEW (a_this)
                              && a_box, CR_BAD_PARAM_ERROR);

        widget = GTK_WIDGET (a_this);
        g_return_val_if_fail (widget, CR_ERROR);

        box = a_box;
        g_return_val_if_fail (box, CR_ERROR);

        if (!box->content)
                return CR_OK;

        if (box->content->content_cache) {
                label = GTK_WIDGET (box->content->content_cache);
                g_return_val_if_fail (label, CR_ERROR);
        }

        g_return_val_if_fail (label, CR_ERROR);
        if (a_put_widgets == TRUE) {
                if (label->parent == NULL)
                        gtk_layout_put (GTK_LAYOUT (a_this), label,
                                        box->inner_edge.x, box->inner_edge.y);

                else
                        gtk_layout_move (GTK_LAYOUT (a_this), label,
                                         box->inner_edge.x,
                                         box->inner_edge.y);
                gtk_widget_show (GTK_WIDGET (label));
        }

        return CR_OK;
}

static enum CRStatus
draw_box (SXBoxView * a_this, SXBox * a_box, gboolean a_put_widgets)
{
        SXBox *cur_box = NULL;
        GtkWidget *widget = NULL;

        g_return_val_if_fail (a_this && SX_IS_BOX_VIEW (a_this)
                              && a_box, CR_BAD_PARAM_ERROR);

        widget = GTK_WIDGET (a_this);
        g_return_val_if_fail (widget, CR_ERROR);

        if (!PRIVATE (a_this)->gc) {
                PRIVATE (a_this)->gc = gdk_gc_new
                        (GDK_DRAWABLE (GTK_LAYOUT (a_this)->bin_window));
                g_return_val_if_fail (PRIVATE (a_this)->gc, CR_ERROR);

                gdk_gc_copy (PRIVATE (a_this)->gc,
                             GTK_WIDGET (a_this)->style->base_gc[widget->
                                                                 state]);
        }

        for (cur_box = a_box; cur_box; cur_box = cur_box->next) {
                /*draw_margins (a_this, cur_box) ; */
                if (!cur_box->style)
                        continue;
                draw_paddings (a_this, cur_box);
                draw_inner_box (a_this, cur_box, a_put_widgets);
                draw_borders (a_this, cur_box);

                if (cur_box->children) {
                        draw_box (a_this, cur_box->children, a_put_widgets);
                }
        }

        return CR_OK;
}

/**********************************
 *Public funtions
 **********************************/

GType
sx_box_view_get_type (void)
{
        static GType type = 0;

        if (type == 0) {
                static const GTypeInfo type_info = {
                        sizeof (SXBoxViewClass),
                        NULL, NULL,
                        (GClassInitFunc) sx_box_view_class_init,
                        NULL, NULL,
                        sizeof (SXBoxView),
                        0,
                        (GInstanceInitFunc) sx_box_view_init
                };

                type = g_type_register_static (GTK_TYPE_LAYOUT,
                                               "SXBoxView", &type_info, 0);
        }

        return type;
}

SXBoxView *
sx_box_view_new (CRCascade * a_cascade, xmlDoc * a_xml_doc)
{
        enum CRStatus status = CR_OK;
        SXGdi *gtk_di = NULL ;
        SXBoxView *result = NULL;
        SXBoxModel *box_model = NULL;

        g_return_val_if_fail (a_cascade && PRIVATE (a_cascade), NULL);

        sx_lay_eng_init ();
        gtk_di = sx_gtk_di_new () ;
        if (!gtk_di) {
                cr_utils_trace_info ("Could not instanciate "
                                     "gtk device interface") ;
                return NULL ;
        }
        result = g_object_new (SX_TYPE_BOX_VIEW, NULL);
        PRIVATE (result)->layeng = sx_lay_eng_new (gtk_di);
        if (!PRIVATE (result)->layeng) {
                cr_utils_trace_info
                        ("Could not instanciate the layout engine. "
                         "The system may be out of memory");
                gtk_widget_destroy (GTK_WIDGET (result));
                return NULL;
        }
        status = sx_lay_eng_create_box_model (PRIVATE (result)->layeng,
                                              a_xml_doc, a_cascade,
                                              &box_model);
        if (status != CR_OK) {
                cr_utils_trace_info ("could not build the annotated doc");
                goto cleanup;
        }

        if (box_model) {
                box_model->box.inner_edge.width = 800;
                box_model->box.inner_edge.max_width = 800;
                box_model->box.inner_edge.width = 600;

                sx_box_view_set_box_model (result, box_model);
                gtk_layout_set_size (GTK_LAYOUT (result), 1024, 768);
                g_signal_connect (G_OBJECT (result),
                                  "map-event",
                                  (GCallback) map_event_cb, NULL);
                g_signal_connect (G_OBJECT (result),
                                  "expose-event",
                                  (GCallback) expose_event_cb, NULL);
                return result;
        }
      cleanup:
        if (box_model) {
                sx_box_destroy ((SXBox *) box_model);
                box_model = NULL;
        }

        if (result) {
                gtk_object_destroy (GTK_OBJECT (result));
        }

        return NULL;
}

/**
 *Instanciates a SXBoxView from a CSS and an XML document.
 *@param a_xml_path the path to the xml document
 *@param a_user_css_path the path to the user CSS. If it's NULL,
 *this parameter is just ignored.
 *@param a_author_css_path the path to the author css. If it's NULL,
 *this parameter is just ignored.
 *@param a_ua_css_path the path to a user specified ua css.
 *is the parameter @a_use_default_ua_sheet is set to TRUE, this
 *parameter is just ignored.
 *@param a_is_html whether the xml input file is html or not.
 *If TRUE, use the html parser to parse the xml input and use
 *the default html4 css as ua_sheet.
 *@param a_use_default_ua_sheet if TRUE, forces to use the sewfox
 *default ua sheet as ua sheet. The param a_ua_css_path is then
 *ignored.
 *@return the newly instanciated SXBoxView or
 *NULL in case of an error.
 */
SXBoxView *
sx_box_view_new_from_xml_css_paths (const guchar * a_xml_path,
                                    const guchar * a_user_css_path,
                                    const guchar * a_author_css_path,
                                    const guchar * a_ua_css_path,
                                    gboolean a_is_html,
                                    gboolean a_use_default_ua_sheet)
{
        enum CRStatus status = CR_OK;
        CRStyleSheet *ua_sheet = NULL,
                *user_sheet = NULL,
                *author_sheet = NULL ;
        CRCascade *cascade = NULL;
        SXBoxView *result = NULL;
        xmlDoc *xml_doc = NULL;

        g_return_val_if_fail (a_xml_path, NULL);

        if (a_use_default_ua_sheet == TRUE) {
                status = sx_box_view_get_default_stylesheet 
                        (&ua_sheet, 
                         a_is_html);
                if (status != CR_OK || !ua_sheet) {
                        return NULL;
                }
        } else {
                if (a_ua_css_path) {
                        status = cr_om_parser_simply_parse_file
                                (a_ua_css_path, CR_ASCII, 
                                 &ua_sheet);
                        if (status != CR_OK) {
                                if (status != CR_OK) {
                                        cr_utils_trace_info 
                                                ("Could not parse "
                                                 "ua sheet");
                                }
                        }
                }
        }
        if (a_user_css_path) {
                status = cr_om_parser_simply_parse_file
                        (a_user_css_path, CR_ASCII, &user_sheet);
                if (status != CR_OK) {
                        cr_utils_trace_info ("Could not parse user sheet");
                        goto cleanup ;
                }
        }
        if (a_author_css_path) {
                status = cr_om_parser_simply_parse_file
                        (a_author_css_path, CR_ASCII, &author_sheet);
        }
        if (status != CR_OK) {
                cr_utils_trace_info ("Could not parse author sheet");
        }
        cascade = cr_cascade_new (author_sheet,
                                  user_sheet,
                                  ua_sheet);
        if (!cascade) {
                cr_utils_trace_info ("Could not instanciate "
                                     "the cascade");
                goto cleanup;
        }
        ua_sheet = user_sheet = NULL;
        if (!a_is_html) {
                xml_doc = xmlParseFile (a_xml_path);
        } else {
                xml_doc = htmlParseFile (a_xml_path, "UTF-8");
        }
        if (!xml_doc) {
                cr_utils_trace_info ("Could not parse xml file");
                goto cleanup;
        }
        result = sx_box_view_new (cascade, xml_doc);
        if (!result)
                goto cleanup;

        return result;

      cleanup:
        if (ua_sheet) {
                cr_stylesheet_unref (ua_sheet);
                ua_sheet = NULL;
        }
        if (user_sheet) {
                cr_stylesheet_unref (user_sheet);
                user_sheet = NULL;
        }
        if (cascade) {
                cr_cascade_unref (cascade);
                cascade = NULL;
        }
        if (xml_doc) {
                xmlFreeDoc (xml_doc);
                xml_doc = NULL;
        }
        return NULL;
}

/**
 *Instanciates a SXBoxView from a CSS and an XML document that
 *reside in memory.
 *@param a_xml_buf the buffer that contains the xml document
 *@param a_css_buf the buffer that contains the CSS
 *@return the newly instanciated SXBoxView or NULL in case of an error.
 */
SXBoxView *
sx_box_view_new_from_xml_css_bufs (const guchar * a_xml_buf,
                                   const guchar * a_css_buf, gboolean is_html)
{
        enum CRStatus status = CR_OK;
        CRStyleSheet *sheet = NULL;
        xmlDoc *xml_doc = NULL;
        SXBoxView *result = NULL;
        CRCascade *cascade = NULL;
        gulong len = 0;

        len = strlen (a_css_buf);
        status = cr_om_parser_simply_parse_buf (a_css_buf, len, CR_UTF_8,
                                                &sheet);
        if (status != CR_OK || !sheet) {
                cr_utils_trace_info ("Could not parse css buf");
                status = CR_ERROR;
                goto cleanup;
        }
        cascade = cr_cascade_new (sheet, NULL, NULL);

        if (!cascade) {
                cr_utils_trace_info ("could not create the cascade");
                cr_utils_trace_info ("The system is possibly out of memory");
                goto cleanup;
        }
        sheet = NULL;

        len = strlen (a_xml_buf);
        if (!is_html) {
                xml_doc = xmlParseMemory (a_xml_buf, len);
        } else {
                xml_doc = htmlParseDoc ((xmlChar*)a_xml_buf, "UTF-8");
        }
        if (!xml_doc) {
                cr_utils_trace_info ("Could not parse xml buf");
                status = CR_ERROR;
                goto cleanup;
        }

        result = sx_box_view_new (cascade, xml_doc);
        return result;

      cleanup:
        if (sheet) {
                cr_stylesheet_destroy (sheet);
                sheet = NULL;
        }

        if (xml_doc) {
                xmlFreeDoc (xml_doc);
                xml_doc = NULL;
        }

        if (cascade) {
                cr_cascade_destroy (cascade);
                cascade = NULL;
        }

        return NULL;
}

/**
 *Instanciates a SXBoxView from a box model
 *@param a_box_root the box model
 *@return the newly instanciated SXBoxView or NULL in case of an error.
 */
SXBoxView *
sx_box_view_new_from_bm (SXBoxModel * a_box_root)
{
        SXGdi * gtk_di = NULL ;
        SXBoxView *result = NULL;

        result = g_object_new (SX_TYPE_BOX_VIEW, NULL);
        g_return_val_if_fail (result, NULL);

        gtk_di = sx_gtk_di_new () ;
        if (!gtk_di) {
                cr_utils_trace_info ("Could not instanciate "
                                     "the gtk device interface") ;
                return NULL ;
        }
        sx_box_view_set_box_model (result, a_box_root);
        gtk_layout_set_size (GTK_LAYOUT (result), 1024, 768);
        g_signal_connect (G_OBJECT (result),
                          "expose-event", (GCallback) expose_event_cb, NULL);

        PRIVATE (result)->layeng = sx_lay_eng_new (gtk_di);
        if (!PRIVATE (result)->layeng) {
                cr_utils_trace_info
                        ("Could not instanciate the layout engine. "
                         "The system may be out of memory");
                gtk_widget_destroy (GTK_WIDGET (result));
                return NULL;
        }
        return result;
}

/**
 *Parses and returns the default sheet (a.k.a user agent sheet) 
 *associated to SEWFOX.
 *@param a_this the current instance of #SXBoxView
 *@param a_sheet out parameter the parsed default stylesheet if any
 *@return CR_OK upon succesful completion, an error code otherwise.
 */
enum CRStatus
sx_box_view_get_default_stylesheet (CRStyleSheet ** a_sheet, 
                                    gboolean is_html)
{
        enum CRStatus status = CR_OK;
        CRStyleSheet *sheet = NULL;
        gchar *default_css_path = NULL;
        const gchar *xml_default = "sewfox-default.css";
        const gchar *html_default = "sewfox-default-html.css";
        const gchar *default_file = NULL;

        g_return_val_if_fail (a_sheet, CR_BAD_PARAM_ERROR);

        if (!is_html) {
                default_file = xml_default;
        } else {
                default_file = html_default;
        }
        default_css_path = g_strjoin ("/",
                                      SEWFOX_DATA_DIR, default_file, NULL);

        g_return_val_if_fail (default_css_path, CR_OUT_OF_MEMORY_ERROR);

        if (g_file_test (default_css_path,
                         G_FILE_TEST_EXISTS | G_FILE_TEST_IS_REGULAR) 
            != TRUE)
        {
                g_warning ("Could not find default CSS : %s",
                           default_css_path);
                status = CR_FILE_NOT_FOUND_ERROR ;
                goto cleanup ;
        }
        status = cr_om_parser_simply_parse_file (default_css_path, 
                                                 CR_ASCII, &sheet);
        if (status != CR_OK) {
                if (sheet) {
                        cr_stylesheet_unref (sheet);
                        sheet = NULL;
                }
                goto cleanup ;
        }
        *a_sheet = sheet;

 cleanup:
        if (default_css_path) {
                g_free (default_css_path) ;
                default_css_path = NULL ;
        }
        return status;
}

enum CRStatus
sx_box_view_set_box_model (SXBoxView * a_this, SXBoxModel * a_box_model)
{
        g_return_val_if_fail (a_this, CR_BAD_PARAM_ERROR);

        if (PRIVATE (a_this)->box_model) {
                if (sx_box_unref ((SXBox *) PRIVATE (a_this)->box_model) ==
                    TRUE)
                        PRIVATE (a_this) = NULL;
        }

        PRIVATE (a_this)->box_model = a_box_model;
        if (a_box_model)
                sx_box_ref ((SXBox *) a_box_model);

        return CR_OK;
}

enum CRStatus
sx_box_view_update_box_model_attrs (SXBoxView * a_this)
{
        enum CRStatus status = CR_OK;
        CRCascade *cascade = NULL;

        g_return_val_if_fail (a_this && PRIVATE (a_this)
                              && PRIVATE (a_this)->layeng
                              && PRIVATE (a_this)->box_model,
                              CR_BAD_PARAM_ERROR);

        cascade = sx_lay_eng_get_cascade (PRIVATE (a_this)->layeng);
        g_return_val_if_fail (cascade, CR_ERROR);

        status = sx_lay_eng_update_box_tree
                (PRIVATE (a_this)->layeng, cascade,
                 PRIVATE (a_this)->box_model->box.children);

        status = sx_lay_eng_layout_box_tree (PRIVATE (a_this)->layeng,
                                             PRIVATE (a_this)->box_model->box.
                                             children);
        draw_box (SX_BOX_VIEW (a_this),
                  ((SXBox *) PRIVATE (SX_BOX_VIEW (a_this))->box_model)->
                  children, TRUE);
        return status;
}

enum CRStatus
sx_box_view_update_box_model_attrs_from_css (SXBoxView * a_this,
                                             guchar * a_author_sheet_path,
                                             guchar * a_user_sheet_path,
                                             guchar * a_ua_sheet_path,
                                             enum CREncoding a_encoding)
{
        enum CRStatus status = CR_OK;
        CRCascade *cascade = NULL;
        CRStyleSheet *sheets[NB_ORIGINS] = { 0 };
        guchar *paths[3] = { 0 };
        gint i = 0;

        g_return_val_if_fail (a_this && PRIVATE (a_this), CR_BAD_PARAM_ERROR);

        cascade = sx_box_view_get_cascade (a_this);
        g_return_val_if_fail (cascade, CR_ERROR);

        paths[ORIGIN_UA] = a_ua_sheet_path;
        paths[ORIGIN_USER] = a_user_sheet_path;
        paths[ORIGIN_AUTHOR] = a_author_sheet_path;

        for (i = ORIGIN_UA; i < NB_ORIGINS; i++) {
                if (paths[i] && strcmp (paths[i], ""))
                        status = cr_om_parser_simply_parse_file (paths[i],
                                                                 a_encoding,
                                                                 &sheets[i]);
                if (sheets[i]) {
                        status = cr_cascade_set_sheet (cascade, sheets[i], i);
                        if (status != CR_OK) {
                                cr_utils_trace_info
                                        ("setting a sheet in the cascade failed");
                        } else {
                                sheets[i] = NULL;
                        }
                }
        }
        status = sx_box_view_update_box_model_attrs (a_this);
        cascade = NULL;
        for (i = 0; i < 3; i++) {
                if (sheets[i]) {
                        cr_stylesheet_unref (sheets[i]);
                        sheets[i] = NULL;
                }
        }
        return status;
}

enum CRStatus
sx_box_view_layout (SXBoxView * a_this)
{

        g_return_val_if_fail (a_this && SX_IS_BOX_VIEW (a_this)
                              && PRIVATE (a_this)->box_model,
                              CR_BAD_PARAM_ERROR);

        sx_lay_eng_layout_box_tree (PRIVATE (a_this)->layeng,
                                    PRIVATE (a_this)->box_model->box.
                                    children);
        /*sx_bo_dump_to_file (PRIVATE (a_this)->box_model->box.children,
           0, stdout) ; */

        return CR_OK;
}

enum CRStatus
sx_box_view_get_box_model (SXBoxView * a_this, SXBoxModel ** a_box_model)
{
        g_return_val_if_fail (a_this && PRIVATE (a_this), CR_BAD_PARAM_ERROR);

        *a_box_model = PRIVATE (a_this)->box_model;
        return CR_OK;
}

/**
 *Getter of the cascade used for the rendering
 *@param a_this the current instance of #SXBoxView
 *@return the cascade or NULL in case of an error.
 */
CRCascade *
sx_box_view_get_cascade (SXBoxView * a_this)
{
        g_return_val_if_fail (a_this && SX_IS_BOX_VIEW (a_this)
                              && PRIVATE (a_this)
                              && PRIVATE (a_this)->layeng, NULL);

        return sx_lay_eng_get_cascade (PRIVATE (a_this)->layeng);
}

void
sx_box_view_dispose (GObject * a_this)
{
        SXBoxView *self = NULL;

        g_return_if_fail (a_this && SX_IS_BOX_VIEW (a_this));

        self = SX_BOX_VIEW (a_this);
        g_return_if_fail (self && PRIVATE (self));

        if (PRIVATE (self)->dispose_has_run == TRUE)
                return;
        if (PRIVATE (self)->box_model) {
                sx_box_unref ((SXBox *) PRIVATE (self)->box_model);
                PRIVATE (self)->box_model = NULL;
        }
        if (PRIVATE (self)->gc) {
                gdk_gc_unref (PRIVATE (self)->gc);
                PRIVATE (self)->gc = NULL;
        }
        PRIVATE (self)->dispose_has_run = TRUE;
        if (gv_parent_class && G_OBJECT_CLASS (gv_parent_class)->dispose) {
                G_OBJECT_CLASS (gv_parent_class)->dispose (a_this);
        }
}

void
sx_box_view_finalize (GObject * a_this)
{
        SXBoxView *view = SX_BOX_VIEW (a_this);

        if (PRIVATE (view)) {
                g_free (PRIVATE (view));
                PRIVATE (view) = NULL;
        }
}
