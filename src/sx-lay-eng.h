/* -*- Mode: C; indent-tabs-mode:nil; c-basic-offset: 8-*- */

/*
 * This file is part of the SEWFOX project
 *
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2.1 of the 
 * GNU Lesser General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the 
 * GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 *Look at the COPYRIGHTS file for copyrights information
 *
 */

#ifndef __SX_LAY_ENG_H__
#define __SX_LAY_ENG_H__

#include <libxml/tree.h>
#include <libcroco/cr-utils.h>
#include <libcroco/cr-cascade.h>
#include "sx-gdi.h"
#include "sx-box.h"

/**
 *@file
 *the declaration of the #SXLayEng class.
 */

G_BEGIN_DECLS typedef struct _SXLayEngPriv SXLayEngPriv;

/**
 *The abstraction of the Layout engine of libcroco.
 */
typedef struct {
        SXLayEngPriv *priv;
} SXLayEng;

void sx_lay_eng_init (void);

SXLayEng *sx_lay_eng_new (SXGdi * a_gdi);

enum CRStatus sx_lay_eng_create_box_model (SXLayEng * a_this,
                                           xmlDoc * a_xml_doc,
                                           CRCascade * a_cascade,
                                           SXBoxModel ** a_box_model);

enum CRStatus sx_lay_eng_update_box_tree (SXLayEng * a_this,
                                          CRCascade * a_cascade,
                                          SXBox * a_box_tree);

enum CRStatus sx_lay_eng_layout_box_tree (SXLayEng * a_this,
                                          SXBox * a_box_tree);

CRCascade *sx_lay_eng_get_cascade (SXLayEng * a_this);

void sx_lay_eng_destroy (SXLayEng * a_this);

G_END_DECLS
#endif /*__SX_LAYOUT_ENG_H__*/
