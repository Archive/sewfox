/* -*- Mode: C; indent-tabs-mode:nil; c-basic-offset: 8-*- */

/*
 * This file is part of The SEWFOX project
 *
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2.1 of 
 * the GNU Lesser General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the 
 * GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 *See the COPYRIGHTS file for copyrights information.
 */

/*
 *$Id$
 */
#ifndef __SX_BOX_VIEW__
#define __SX_BOX_VIEW__

#include <gtk/gtk.h>
#include <libcroco/cr-cascade.h>
#include "sx-box.h"             /*the box model */

G_BEGIN_DECLS
#define SX_TYPE_BOX_VIEW            (sx_box_view_get_type ())
#define SX_BOX_VIEW(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), SX_TYPE_BOX_VIEW, SXBoxView))
#define SX_BOX_VIEW_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), SX_TYPE_BOX_VIEW, SXBoxViewClass))
#define SX_IS_BOX_VIEW(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), SX_TYPE_BOX_VIEW))
#define SX_IS_BOX_VIEW_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), SX_TYPE_BOX_VIEW))
#define SX_BOX_VIEW_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), SX_TYPE_BOX_VIEW, SXBoxViewClass))
typedef struct _SXBoxView SXBoxView;
typedef struct _SXBoxViewClass SXBoxViewClass;
typedef struct _SXBoxViewPriv SXBoxViewPriv;

struct _SXBoxView {
        GtkLayout parent_widget;
        SXBoxViewPriv *priv;
};

struct _SXBoxViewClass {
        GtkLayoutClass parent_class;
};

GType sx_box_view_get_type (void);

SXBoxView *sx_box_view_new (CRCascade * a_cascade, xmlDoc * a_xml_doc);

SXBoxView *sx_box_view_new_from_xml_css_bufs (const guchar * a_xml_buf,
                                              const guchar * a_css_buf,
                                              gboolean is_html);

enum CRStatus sx_box_view_update_box_model_attrs (SXBoxView * a_this);

enum CRStatus sx_box_view_update_box_model_attrs_from_css (SXBoxView * a_this,
                                                           guchar *
                                                           a_author_sheet_path,
                                                           guchar *
                                                           a_user_sheet_path,
                                                           guchar *
                                                           a_ua_sheet_path,
                                                           enum CREncoding
                                                           a_encoding);

enum CRStatus sx_box_view_get_box_model (SXBoxView * a_this,
                                         SXBoxModel ** a_box_model);

enum CRStatus sx_box_view_set_box_model (SXBoxView * a_this,
                                         SXBoxModel * a_box_model);

SXBoxView *sx_box_view_new_from_bm (SXBoxModel * a_box_root);

enum CRStatus sx_box_view_layout (SXBoxView * a_this);

void sx_box_view_dispose (GObject * a_this);

void sx_box_view_finalize (GObject * a_this);

SXBoxView * sx_box_view_new_from_xml_css_paths (const guchar * a_xml_path,
                                                const guchar * a_user_css_path,
                                                const guchar * a_author_css_path,
                                                const guchar * a_ua_css_path,
                                                gboolean a_is_html,
                                                gboolean a_use_default_ua_sheet) ;

CRCascade *sx_box_view_get_cascade (SXBoxView * a_this);

enum CRStatus sx_box_view_get_default_stylesheet (CRStyleSheet ** a_sheet,
                                                  gboolean is_html);

G_END_DECLS
#endif
