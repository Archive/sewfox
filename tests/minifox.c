/* -*- Mode: C; indent-tabs-mode:nil; c-basic-offset: 8-*- */

/*
 * This file is part of the SEWFOX project
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2.1 of 
 * the GNU Lesser General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the 
 * GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * Initial Author: Stephane Bonhomme
 * Maintainer: Dodji Seketeli.
 */

#include <stdio.h>
#include <string.h>
#include <libxml/parser.h>
#include <libxml/tree.h>
#include <sx-box-view.h>

static void display_usage (void);

static GtkWidget *get_contextual_menu (SXBoxView * a_viewport);

static gboolean delete_event_cb (GtkWidget * widget,
                                 GdkEvent * event, gpointer data);

struct MinifoxOptions {
        gboolean display_usage ;
        gboolean use_default_ua_css ;
        gchar *ua_css ;
        gchar *user_css ;
        gchar *author_css ;        
        gchar *xml_doc ;
        gchar **remaining_cmd_line ;
} ;

static struct MinifoxOptions *
new_minifox_options (void)
{
        struct MinifoxOptions *result = NULL ;

        result = g_try_malloc (sizeof (struct MinifoxOptions)) ;
        if (!result) {
                cr_utils_trace_info ("System is Out of memory") ;
                return NULL ;
        }
        memset (result, 0, sizeof (struct MinifoxOptions)) ;
        result->use_default_ua_css = TRUE ;
        result->display_usage =FALSE ;
        return result ;
}

static void
parse_cmd_line (int a_argc, char **a_argv,
                struct MinifoxOptions *a_options)
{
        gint i = 0 ;

        g_return_if_fail (a_argv && a_options) ;

        if (a_argc == 1)
                return ;
        i= 1 ;
        while (a_argv[i][0] == '-') {
                if (!strcmp (a_argv[i], "--no-uacss")) {
                        a_options->use_default_ua_css = FALSE ;
                        i++ ;
                } else if (!strcmp (a_argv[i], "--uacss")){
                        if (i >= a_argc) {
                                a_options->display_usage = TRUE ;
                                return ;
                        }
                        i++ ;
                        a_options->ua_css = a_argv[i] ;
                        a_options->use_default_ua_css = FALSE ;
                        i++ ;
                } else if (!strcmp (a_argv[i], "--authorcss")) {
                        if (i >= a_argc) {
                                a_options->display_usage = TRUE ;
                                return ;
                        }
                        i++ ;
                        a_options->author_css = a_argv[i] ;
                        i++ ;
                } else if (!strcmp (a_argv[i], "--help")
                           | !strcmp (a_argv[i], "-h")) {
                        a_options->display_usage = TRUE ;
                        return ;
                }
        }
        if (i >= a_argc)
                return ;
        a_options->xml_doc = a_argv[i] ;
        i++ ;
        if (i >= a_argc)
                return ;
        a_options->user_css = a_argv[i] ;        
}

static gboolean
delete_event_cb (GtkWidget * widget, 
                 GdkEvent * event, 
                 gpointer data)
{
        gtk_main_quit ();
        return FALSE;
}

static void
dump_box_model_menuitem_activate_cb (GtkWidget * a_menuitem,
                                     gpointer a_user_data)
{
        SXBoxModel *box_model = NULL;
        SXBoxView *viewport = a_user_data;

        g_return_if_fail (a_menuitem && GTK_IS_WIDGET (a_menuitem)
                          && SX_IS_BOX_VIEW (viewport));

        sx_box_view_get_box_model (viewport, &box_model);
        if (box_model) {
                sx_box_dump_to_file ((SXBox *) box_model, 0, stdout);
        } else {
                cr_utils_trace_info ("Got NULL box_model");
        }

}

static gboolean
button_press_event_cb (GtkWidget * a_viewport,
                       GdkEvent * a_event, gpointer * a_user_data)
{
        GtkWidget *menu = NULL,
                *window = NULL;
        GdkEventButton *button_event = NULL;

        g_return_val_if_fail (a_event && a_viewport
                              && SX_BOX_VIEW (a_viewport), FALSE);

        window = GTK_WIDGET (a_user_data);
        g_return_val_if_fail (window && GTK_IS_WINDOW (window), FALSE);

        switch (a_event->type) {
        case GDK_BUTTON_PRESS:
                if (a_event->button.button == 3) {
                        menu = get_contextual_menu (SX_BOX_VIEW (a_viewport));
                        if (!menu)
                                break;
                        button_event = (GdkEventButton *) a_event;
                        gtk_menu_popup (GTK_MENU (menu),
                                        NULL, NULL, NULL,
                                        window, button_event->button,
                                        button_event->time);
                        return TRUE;
                }
                break;
        default:
                break;
        }
        return FALSE;
}

static void
display_usage (void)
{
        g_print ("usage : minifox <xmlfile> [cssfile]\n");
}


static GtkWidget *
get_contextual_menu (SXBoxView * a_viewport)
{
        static GtkWidget *menu = NULL;
        GtkWidget *menuitem = NULL;

        g_return_val_if_fail (a_viewport
                              && SX_IS_BOX_VIEW (a_viewport), NULL);

        if (!menu) {
                menu = gtk_menu_new ();
                menuitem = gtk_menu_item_new_with_label ("Dump Box model");
                gtk_widget_show (menuitem);
                g_signal_connect (G_OBJECT (menuitem),
                                  "activate",
                                  G_CALLBACK
                                  (dump_box_model_menuitem_activate_cb),
                                  a_viewport);
                gtk_menu_shell_append (GTK_MENU_SHELL (menu), menuitem);
        }
        return menu;
}

int
main (int argc, char **argv)
{
        gboolean is_html = FALSE;
        struct MinifoxOptions *options = NULL ;
        GtkWidget *window = NULL;
        GtkWidget *scroll = NULL;
        SXBoxView *viewport = NULL;
        const gchar *ext = NULL, 
                *xml_file = NULL ;

        gtk_init (&argc, &argv);

        options = new_minifox_options () ;
        if (!options) {
                cr_utils_trace_info ("Could not instanciate MinifoxOptions") ;
                return -1 ;
        }
        parse_cmd_line (argc, argv, options) ;

        if (!options->xml_doc 
            || options->display_usage == TRUE) {
                display_usage () ;
        }
        xml_file = options->xml_doc ;


        window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
        gtk_window_set_default_size (GTK_WINDOW (window), 500, 300);
        gtk_window_set_resizable (GTK_WINDOW (window), TRUE);
        g_signal_connect (G_OBJECT (window), "delete-event",
                          G_CALLBACK (delete_event_cb), NULL);

        scroll = gtk_scrolled_window_new (NULL, NULL);
        gtk_container_add (GTK_CONTAINER (window), scroll);
        if (options->user_css) {
                g_print ("Rendering xml: %s - user css: %s \n",
                         xml_file, 
                         options->user_css);
        } else {
                g_print ("Rendering xml: %s - no user css\n", 
                         xml_file) ;
        }

        ext = strrchr (argv[1], '.');
        if (ext) {
                is_html = !strcmp (".html", ext);
        }
        viewport = sx_box_view_new_from_xml_css_paths
                (xml_file, options->user_css,
                 options->author_css,
                 options->ua_css, is_html,
                 options->use_default_ua_css) ;
        if (!viewport) {
                cr_utils_trace_info ("Could not instanciate the viewport");
                return -1;
        }
        gtk_container_add (GTK_CONTAINER (scroll), GTK_WIDGET (viewport));
        g_signal_connect (G_OBJECT (viewport),
                          "button-press-event",
                          G_CALLBACK (button_press_event_cb), window);
        gtk_widget_show_all (window);
        gtk_main ();

        return 0;
}
